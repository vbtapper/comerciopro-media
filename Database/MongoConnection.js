var config = require('../config');
var Mongoose = require('mongoose');

var promise = Mongoose.connect(config.database.stagingURI, {
    useMongoClient: true,
});
    
Mongoose.Promise = global.Promise;    
const db = Mongoose.connection;

db.on('error', function callback() {
    console.error.bind(console, 'connection error')
});

db.once('open', function callback() {
    console.log("Connection with database succeeded.");
});
    