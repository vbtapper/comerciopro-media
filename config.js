module.exports = {
    Host : {
        baseTestURL: 'http://media.nossapps.com',
        baseLiveURL: 'https://media.comerciopro.com',
        key: '345634252492049028459294910950205912934DFHE244',
        baseDevURL : 'http://localhost:3026',
        port: 3000
    },
    utils : {
        randomID: '12345678909876543212345678909876543211234567890'
    },
    Admin: {
        scope: ['Admin'],
        accessKey: '1101029293838474756565567447833892920101'
    },
    API: {
        devURL: 'http://localhost:3016',
        stagingURL: 'http://apistaging.comerciopro.com',
        productionURL: 'https://api.comerciopro.com',
        key: '',
    },
    database: {
        stagingURI: 'mongodb://stguser:Eusouhumano12@ds119014.mlab.com:19014/cmcprmd'
    },
    Images : {
        randomID: '0123456789'
    }
}