#!/usr/local/bin/node
//'use strict';

const Hapi              = require('hapi');
const fs                = require('fs');
const Joi               = require('joi');
const Boom              = require('boom');
const Inert             = require('inert');
const Path              = require('path');
const AuthBearer        = require('hapi-auth-bearer-token');
const Client            = require('node-rest-client').Client;
const Q                 = require('q');
const schedule          = require('node-schedule');
const randtoken         = require('rand-token').generator();

const ImageModel        = require('./Models/ImageModel').DocumentSchema;
const config            = require('./config.js');


function CheckClientToken(token) {
    var client = new Client();
    
    var deferred = Q.defer();
    var args = {
        parameters: {
            sessiontoken: token,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : 'Bearer ' + token,
            "Content-Type": "application/json",
        }
    };
    client.get(config.API.stagingURL + "/client/checkclient", args, function (CustomerInfo, response) {
        if(CustomerInfo['statusCode'] == 200) {
            deferred.resolve(CustomerInfo);
        }
        else {
            deferred.reject(CustomerInfo);
        }
    })
    
    return deferred.promise;
}

const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'documents')
            }
        }
    }
});

server.connection({ port: config.Host.port });

//registering

server.register(AuthBearer, (err) => {
    if (err) {
        throw err;
    }

    server.auth.strategy('client', 'bearer-access-token', {
        allowQueryToken: true,              // optional, true by default
        allowMultipleHeaders: true,        // optional, false by default
        accessTokenName: 'access_token',    // optional, 'access_token' by default
        validateFunc: function (token, callback) {          
            var request = this;  
            CheckClientToken(token)
            .then(FoundClient => {
                return callback(null, true, FoundClient);
            })
            .catch(err => {
                return callback(null, false);
            })
        }
    });

    server.route([
        {
            /**
             * Create an account for ekumbu customer
             * params
             *      - file : the name of the file to be stored
             *      curl -F file=@tapr2.pdf -F "trackingID=33432434342" localhost:3007/post -H "Authorization:Bearer ddA424aD2Da7aWs9D22A Content-Type: application/json charset=UTF-8"
             */
            method: 'POST',
            path: '/post',
            config: {
                auth: 'client',
                payload: {
                    output: 'stream',
                    parse: true,
                },
                handler: function (request, reply) {
                    
                    console.log('here')
                    var data = request.payload;
                    var fileNameforExtension = data.file.hapi.filename;
                    var fileExtension = fileNameforExtension.substring(fileNameforExtension.length - 4, fileNameforExtension.length);
                    var contentTypeParam = data.file.hapi.headers['content-type'];

                    if (data.file) {

                        var name = data.file.hapi.filename;
                        var path = __dirname + "/" + name;
                        var file = fs.createWriteStream(path);
                        file.on('error', function (err) { 
                            return reply(Boom.badData); 
                        });

                        data.file.pipe(file);
                        data.file.on('end', function (err) { 
                            if(err) {
                                return reply(Boom.badData); 
                            }
                            else {
                                //TODO fs.readFileSync read first the file and then save it

                                fs.readFile(file.path, function(err, data) {
                                    console.log(err)
                                    if(err) {
                                        var rspInfo = {
                                            statusCode: 500,
                                            error: 'Internal Error',
                                            message: 'an internal error occured while saving the image. please check the logs for details',
                                        }
                                        return reply(rspInfo);
                                    }
                                    else {
                                        var newDocInfo = {
                                            file: {
                                                imageID : randtoken.generate(20, config.Images.randomID),
                                                data : data,
                                                contentType : contentTypeParam,
                                                extension: fileExtension
                                            }
                                        }
                                        var newDoc = new ImageModel(newDocInfo);
                                        newDoc.save(function(err, nd) {  
                                            fs.unlinkSync(file.path)
                                            if(err) {
                                                return reply(Boom.internal('an internal error occured'));
                                            } 
                                            else {
                                                var rspInfo = {
                                                    statusCode: 200,
                                                    error: null,
                                                    message: 'success',
                                                    fileName : nd.file.imageID
                                                }
                                                return reply(rspInfo);
                                            }
                                        })
                                    }
                                })
                            }
                        });
                    }
                    else {
                        var rspInfo = {
                            statusCode: 403,
                            error: 'Request Error',
                            message: 'File not provided',
                        }
                        return reply(rspInfo);
                    }
                }
            }
        },
        {
            method: 'GET',
            path: '/assets/image/{imageID}', 
            config: {
                handler: function(request, reply) {
                    ImageModel.findOne({ "file.imageID": request.params.imageID}, function(err, doc) {
                        if(err) {
                            console.log(err)
                            return reply(Boom.notFound('content not found'))
                        }
                        else {
                            if(doc == null || doc == undefined) {
                                return reply(Boom.notFound('content not found'))
                            }
                            else {
                                var path = __dirname + "/assets/images/" + doc.file.imageID + doc.file.extension;
                                var options = {
                                    confine: false
                                }

                                if(fs.existsSync(path)) {
                                    return reply.file(path, options).vary('x-magic');
                                    let startTime = new Date(Date.now()+(1440 * 60 * 1000));
                                    var j = schedule.scheduleJob(startTime , function(){
                                        j.cancel();
                                        if(fs.existsSync(path)) {
                                            fs.unlink(path)
                                        }
                                    });
                                }
                                else {
                                    var fd = fs.openSync(path, 'wx');
                                    fs.writeFileSync(fd, doc.file.data);
                                    return reply.file(path, options).vary('x-magic');
                                }
                            }  
                        }
                    })
                }
            }
        },
        {
            method: 'PATCH',
            path: '/remove/{imageID}', 
            config: {
                auth: 'client',
                handler: function(request, reply) {
                    ImageModel.findOneAndRemove({'file.imageID' : request.params.imageID }, function(err, data) {
                        if(err) {
                            console.log(err)
                            return reply(Boom.internal('an internal error occured while removing the image. Check the logs for details'))
                        }
                        else {
                            var path = __dirname + "/assets/images/" + data.file.imageID + data.file.extension;
                            if(fs.existsSync(path)) {
                                fs.unlink(path)
                                var infoResponse = {
                                    statusCode: 200,
                                    error: null,
                                    message: 'success'
                                }
                                return reply(infoResponse)    
                            }
                            else {
                                var infoResponse = {
                                    statusCode: 200,
                                    error: null,
                                    message: 'success'
                                }
                                return reply(infoResponse)    
                            }
                        }
                    })
                }
            }
        }
        
    ]);

});

server.register(Inert, () => {});

server.start();
console.log('ComercioPro Media listening at port ' + server.info.port);

