const dataBase          = require('../Database/MongoConnection');
const Mongoose          = require('mongoose');
const Schema            = Mongoose.Schema;

var ImageModel = new Schema({
    file: { 
        imageID: {type: String, required: true},
        data: { type: Buffer, required: true }, 
        contentType: {type: String, required: true},
        extension: {type: String, required: true}
    }
});

var images = Mongoose.model('images', ImageModel);
module.exports = {
    DocumentSchema: images
};